package modelo.cliente;

public interface ClienteIFachada {
	
	public void cadastrarCliente (Cliente c) throws Exception ;
	public Cliente loginAcesso (String email , String senha) throws Exception;
	public void alterarSenha(Cliente cliente) throws Exception;
	public Cliente verificarExistencia(String email) ;

}
