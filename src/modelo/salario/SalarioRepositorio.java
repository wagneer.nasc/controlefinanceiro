package modelo.salario;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import entityManagerJPA.EntityManagerUtil;

public class SalarioRepositorio implements SalarioIFachada {

	EntityManager em = EntityManagerUtil.getEntityManager();

	public void adcionarSalario(Salario salario) {

		em.getTransaction().begin();
		em.persist(salario);
		em.getTransaction().commit();
		// em.close();

	}

	@Override
	public void removerSalario(Salario salario) {

		em.getTransaction().begin();
		em.remove(salario);
		em.getTransaction().commit();
		// em.close();
	}

	@Override
	public void atualizarSalario(Salario salario) {
		em.getTransaction().begin();
		em.merge(salario);
		em.getTransaction().commit();
		// em.close();

	}

	public List<Salario> listarSalario(int id) {

		String consulta = "Select s From Salario s join s.cliente c where c.idCliente= :id ORDER BY id_salario DESC";
		TypedQuery<Salario> query = em.createQuery(consulta, Salario.class);
		query.setParameter("id", id);
		query.setMaxResults(1);
		List<Salario> resultado = query.getResultList();

		return resultado;
	}

	 

}
