package modelo.salario;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import modelo.cliente.Cliente;

@Entity
public class Salario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_salario")
	private int id;
	@Column (name= "valor_salario")
	private double valor;
	@Temporal(TemporalType.DATE)
	private Date data;
	@OneToOne
	@JoinColumn(name = "id_cliente")
	private Cliente cliente;
	@Column (name="salario_restante")
	private double salarioRestante ;

	public Salario() {

	}
	public double getSalarioRestante() {
		return salarioRestante;
	}
	public void setSalarioRestante(double salarioRestante) {
		this.salarioRestante = salarioRestante ;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
 
 

 

}
