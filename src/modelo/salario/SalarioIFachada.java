package modelo.salario;

import java.util.List;

 

public interface SalarioIFachada {
	
	public void adcionarSalario(Salario salario);
	public void removerSalario(Salario salario);
	public void atualizarSalario(Salario salario);
	public List<Salario> listarSalario(int id) ;

	 
}
