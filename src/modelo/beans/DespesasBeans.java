package modelo.beans;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

import org.primefaces.PrimeFaces;

import controlador.fachada.FachadaControlador;
import controlador.session.SessionContext;
import entityManagerJPA.EntityManagerUtil;
import modelo.cliente.Cliente;
import modelo.despesa.Despesa;
import modelo.salario.Salario;

@ManagedBean
@SessionScoped
public class DespesasBeans {
	EntityManager em = EntityManagerUtil.getEntityManager();
	Cliente cliente = (Cliente) SessionContext.getInstance().getAttribute("usuariologado");

	private List<Despesa> listaDespesas;
	private List<Despesa> despesaSelecionada;
	private Despesa despesa;
	private String descricaoDespesaSelecionada;
	private Date dataDespesaSelecionada;
	private double valorDespesaSelecionada;
	private List<Salario> salarioAtual;
	private int idSalarioAtual;
	private int idDespesaSelecionada;
	private double salarioRestante;
	private Salario salario;
	private double despesaAtual ;

	public double getDespesaAtual() {
		return despesaAtual;
	}

	public void setDespesaAtual(double despesaAtual) {
		this.despesaAtual = despesaAtual;
	}

	public Salario getSalario() {
		return salario;
	}

	public void setSalario(Salario salario) {
		this.salario = salario;
	}

	public double getSalarioRestante() {
		return salarioRestante;
	}

	public void setSalarioRestante(double salarioRestante) {
		this.salarioRestante = salarioRestante;
	}

	public int getIdDespesaSelecionada() {
		return idDespesaSelecionada;
	}

	public void setIdDespesaSelecionada(int idDespesaSelecionada) {
		this.idDespesaSelecionada = idDespesaSelecionada;
	}

	public List<Despesa> getDespesaSelecionada() {
		return despesaSelecionada;
	}

	public void setDespesaSelecionada(List<Despesa> despesaSelecionada) {
		this.despesaSelecionada = despesaSelecionada;
	}

	public List<Salario> getSalarioAtual() {
		return salarioAtual;
	}

	public void setSalarioAtual(List<Salario> salarioAtual) {
		this.salarioAtual = salarioAtual;
	}

	public int getIdSalarioAtual() {
		return idSalarioAtual;
	}

	public void setIdSalarioAtual(int idSalarioAtual) {
		this.idSalarioAtual = idSalarioAtual;
	}

	public String getDescricaoDespesaSelecionada() {
		return descricaoDespesaSelecionada;
	}

	public void setDescricaoDespesaSelecionada(String descricaoDespesaSelecionada) {
		this.descricaoDespesaSelecionada = descricaoDespesaSelecionada;
	}

	public Date getDataDespesaSelecionada() {
		return dataDespesaSelecionada;
	}

	public void setDataDespesaSelecionada(Date dataDespesaSelecionada) {
		this.dataDespesaSelecionada = dataDespesaSelecionada;
	}

	public double getValorDespesaSelecionada() {
		return valorDespesaSelecionada;
	}

	public void setValorDespesaSelecionada(double valorDespesaSelecionada) {
		this.valorDespesaSelecionada = valorDespesaSelecionada;
	}

	@PostConstruct
	public void pegarIdSalarioAtual() {

		this.salarioAtual = FachadaControlador.getInstanciaSalario().listarSalario(cliente.getId());
		for (Salario salario : salarioAtual) {
			idSalarioAtual = salario.getId();
			salarioRestante = salario.getSalarioRestante();
		}

	}

	public Despesa getDespesa() {
		if (this.despesa == null) {
			this.despesa = new Despesa();
		}
		return despesa;
	}

	public void setDespesa(Despesa despesa) {
		this.despesa = despesa;
	}

	public List<Despesa> getListaDespesas() {

		this.listaDespesas = FachadaControlador.getInstanciaDespesa().listarDespesas(idSalarioAtual);

		return listaDespesas;
	}

	public String cadastrarDespesa() {
		pegarIdSalarioAtual();
		if (despesa != null) {

			despesa.setSalario(em.find(Salario.class, idSalarioAtual));
			FachadaControlador.getInstanciaDespesa().cadastrarDespesa(despesa);
			cadastrarDespesaAtualizarSalarioRestante(despesa.getValor());
		}
		despesa = new Despesa();
		return "/principal/principal.xhtml";
	}

	public String deleteDespesa() {
		pegarIdSalarioAtual();
		String id = SessionContext.getInstance().getParametroId("idDespesa");
		int idDespesa = Integer.parseInt(id);
		Despesa despesa = em.find(Despesa.class, idDespesa);
		FachadaControlador.getInstanciaDespesa().removerDespesa(despesa);

		deletarDespesaAtualizarSalarioRestante(despesa.getValor());

		return "/principal/principal.xhtml";
	}

	public String abrirModalEditarDespesa() {
		pegarIdSalarioAtual();
		String id = SessionContext.getInstance().getParametroId("idDespesa");
		int idDespesa = Integer.parseInt(id);

		this.despesaSelecionada = FachadaControlador.getInstanciaDespesa().listarDespesasSelecionada(idDespesa);
		for (Despesa despesa : despesaSelecionada) {
			idDespesaSelecionada = despesa.getId();
			descricaoDespesaSelecionada = despesa.getDescricao();
			valorDespesaSelecionada = despesa.getValor();
			dataDespesaSelecionada = despesa.getDataDespesa();
		}

		PrimeFaces.current().executeScript("$('.editarDespesa').modal()");

		return null;
	}

	public String editarDespesa() {
	 
		this.despesaSelecionada = FachadaControlador.getInstanciaDespesa().listarDespesasSelecionada(idDespesaSelecionada);
	
		for (Despesa despesa : despesaSelecionada) {
			despesaAtual = despesa.getValor();
		}
		if (valorDespesaSelecionada > despesaAtual) {
			double diferenca =  valorDespesaSelecionada - despesaAtual ;
			salarioRestante = salarioRestante - diferenca ;
		}
		if(valorDespesaSelecionada < despesaAtual) {
			double diferenca = despesaAtual - valorDespesaSelecionada;
			salarioRestante = salarioRestante + diferenca;
		}
		
		this.despesa.setIdDespesa(idDespesaSelecionada);
		this.despesa.setValor(valorDespesaSelecionada);
		this.despesa.setDescricao(descricaoDespesaSelecionada);
		this.despesa.setDataDespesa(dataDespesaSelecionada);
		this.despesa.setSalario(em.find(Salario.class, idSalarioAtual));
		FachadaControlador.getInstanciaDespesa().atualizarDespesa(despesa);
		this.salario =em.find(Salario.class, idSalarioAtual);
		this.salario.setSalarioRestante(salarioRestante);
		FachadaControlador.getInstanciaSalario().atualizarSalario(salario);
		return "/principal/principal.xhtml";
	}

	public void cadastrarDespesaAtualizarSalarioRestante(double valorDespesa) {
		double salarioRestanteAtualizado = salarioRestante - valorDespesa;
		this.salario = em.find(Salario.class, idSalarioAtual);
		this.salario.setSalarioRestante(salarioRestanteAtualizado);
		FachadaControlador.getInstanciaSalario().atualizarSalario(salario);

	}

	public void deletarDespesaAtualizarSalarioRestante(double valorDespesa) {
		double salarioRestanteAtualizado = salarioRestante + valorDespesa;
		this.salario = em.find(Salario.class, idSalarioAtual);
		this.salario.setSalarioRestante(salarioRestanteAtualizado);
		FachadaControlador.getInstanciaSalario().atualizarSalario(salario);

	}
	public void editarDespesaAtualizarSalarioRestante () {
		
		this.salario = em.find(Salario.class, idSalarioAtual);
		this.salario.setSalarioRestante(salarioRestante);
		FachadaControlador.getInstanciaSalario().adcionarSalario(salario);
		
	}

}
