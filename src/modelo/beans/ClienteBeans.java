package modelo.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import controlador.fachada.FachadaControlador;
import controlador.session.SessionContext;
import modelo.cliente.Cliente;

@ManagedBean
@SessionScoped
public class ClienteBeans {
	private Cliente cliente;

	public Cliente getCliente() {

		if (this.cliente == null) {
			this.cliente = new Cliente();
		}
		return this.cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public String logout() {
		SessionContext.getInstance().encerrarSessao();
		return "/inicial/login.xhtml";
	}
	public String logar() {
		cliente = FachadaControlador.getInstanciaCliente().loginAcesso(cliente.getEmail(), cliente.getSenha());
		if (cliente != null) {
			SessionContext.getInstance().setAttribute("usuariologado", cliente);
			return "/principal/principal.xhtml";

		}
		return "/inicial/login.xhtml";
	}
	public String salvarCliente() {
		cliente.getNome();
		cliente.getEmail();
		cliente.getSenha();

		Cliente existeUsuario = FachadaControlador.getInstanciaCliente().verificarExistencia(cliente.getEmail());

		if (existeUsuario == null) {
			System.out.println(" Este email n�o existe, pode cadastrar");

			if (cliente != null) {
				try {
					FachadaControlador.getInstanciaCliente().cadastrarCliente(cliente);
					System.out.println(" cadastrado com sucesso");
					return "login.xhtml";

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} else {
			System.out.println(" Este email j� foi cadastrado");
		}
		return "/inicial/cadastroCliente.xhtml";
	}
}
