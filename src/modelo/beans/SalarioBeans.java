package modelo.beans;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import controlador.fachada.FachadaControlador;
import controlador.session.SessionContext;
import entityManagerJPA.EntityManagerUtil;
import modelo.cliente.Cliente;
import modelo.salario.Salario;

@ManagedBean
@SessionScoped
public class SalarioBeans {

	EntityManager em = EntityManagerUtil.getEntityManager();

	Cliente cliente = (Cliente) SessionContext.getInstance().getAttribute("usuariologado");
	private double salarioAtual;
	private int idSalarioAtual;
	private Date dataSalarioAtual;
	private Salario salario;
	private List<Salario> listaSalario;
	private double salarioRestante;
	//private double salarioAtual;

	/*
	 * public double getUltimoSalario() { return salarioAtual; }
	 * 
	 * public void setUltimoSalario(double ultimoSalario) { this.salarioAtual =
	 * ultimoSalario; }
	 */

	public double getSalarioRestante() {
		return salarioRestante;
	}

	public void setSalarioRestante(double salarioRestante) {
		this.salarioRestante = salarioRestante;
	}

	public List<Salario> getListaSalario() {
		return this.listaSalario;
	}

	public void setListaSalario(List<Salario> listaSalario) {
		this.listaSalario = listaSalario;
	}

	public int getIdSalarioAtual() {
		return idSalarioAtual;
	}

	public void setIdSalarioAtual(int idSalarioAtual) {
		this.idSalarioAtual = idSalarioAtual;
	}

	public Date getDataSalarioAtual() {
		return dataSalarioAtual;
	}

	public void setDataSalarioAtual(Date dataSalarioAtual) {
		this.dataSalarioAtual = dataSalarioAtual;
	}

	public double getSalarioAtual() {
		return salarioAtual;
	}

	public void setSalarioAtual(double salarioAtual) {
		this.salarioAtual = salarioAtual;
	}

	public void setSalario(Salario salario) {
		this.salario = salario;
	}

	@PostConstruct
	public void iniciaSalarioAtual() {
		this.listaSalario = FachadaControlador.getInstanciaSalario().listarSalario(cliente.getId());
		for (Salario salario : listaSalario) {
			salarioAtual = salario.getValor();
			dataSalarioAtual = salario.getData();
			idSalarioAtual = salario.getId();
			salarioRestante = salario.getSalarioRestante();
			
			System.out.println("Meu salario Atual" + salarioAtual);

		}
	}

	public Salario getSalario() {
		if (this.salario == null) {
			this.salario = new Salario();
		}
		return this.salario;
	}

	public String cadastrarSalario() {

		if (cliente != null) {
			if (this.salario != null) {
				salario.setSalarioRestante(salario.getValor());
				this.salario.setCliente(em.find(Cliente.class, cliente.getIdCliente()));
				FachadaControlador.getInstanciaSalario().adcionarSalario(salario);
			}
		}

		return "/principal/principal.xhtml";
	}

	public String editarSalario() {
		
		this.listaSalario = FachadaControlador.getInstanciaSalario().listarSalario(cliente.getId());
		/*
		 * for (Salario salario : listaSalario) { salarioAtual = salario.getValor(); }
		 * if (salarioAtual > salarioAtual) { double diferenca =
		 * salarioAtual - salarioAtual ; salarioRestante = salarioRestante -
		 * diferenca ; } if(salarioAtual < salarioAtual) { double diferenca =
		 * salarioAtual - salarioAtual; salarioRestante = salarioRestante +
		 * diferenca; }
		 */
		this.salario.setId(idSalarioAtual);
		this.salario.setData(dataSalarioAtual);
		this.salario.setValor(salarioAtual); 
		this.salario.setSalarioRestante(salarioRestante);
		this.salario.setCliente(em.find(Cliente.class, cliente.getIdCliente()));
		FachadaControlador.getInstanciaSalario().atualizarSalario(salario);
		
		return "/principal/principal.xhtml";
	}
	

}
