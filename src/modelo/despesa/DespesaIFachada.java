package modelo.despesa;

import java.util.List;

public interface DespesaIFachada {
	public void cadastrarDespesa(Despesa d);
	public List<Despesa> listarDespesas(int id);
	public void removerDespesa(Despesa despesa);
	public List<Despesa> listarDespesasSelecionada(int id) ;
	public void atualizarDespesa(Despesa despesa) ;
}
