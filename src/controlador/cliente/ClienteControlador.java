package controlador.cliente;

import modelo.cliente.Cliente;
import modelo.cliente.ClienteIFachada;
import modelo.fachada.FachadaRepositorio;

public class ClienteControlador implements ClienteIFachada {

	private static FachadaRepositorio repositorio;

	public ClienteControlador() {
		repositorio = new FachadaRepositorio();

	}

	@Override
	public void cadastrarCliente(Cliente c) throws Exception {
		repositorio.cadastrarCliente(c);

	}

	@Override
	public Cliente loginAcesso(String email, String senha) {
		return repositorio.loginAcesso(email, senha);
	}

	 
	public void alterarSenha(Cliente cliente) throws Exception {
		repositorio.alterarSenha(cliente);
		
	}

	public Cliente verificarExistencia(String email) {
		return repositorio.verificarExistencia(email);
	}

}
