package controlador.salario;

import java.util.List;

import modelo.fachada.FachadaRepositorio;
import modelo.salario.Salario;

public class SalarioControlador {

	private FachadaRepositorio repositorio;

	public SalarioControlador() {
		repositorio = new FachadaRepositorio();
	}

	public void adcionarSalario(Salario salario) {
		this.repositorio.adcionarSalario(salario);

	}

	public void removerSalario(Salario salario) {
		this.repositorio.removerSalario(salario);
	}

	public void atualizarSalario(Salario salario) {
		this.repositorio.atualizarSalario(salario);

	}
	public List<Salario> listarSalario(int id) {
		return repositorio.listarSalario(id);
	}

}
